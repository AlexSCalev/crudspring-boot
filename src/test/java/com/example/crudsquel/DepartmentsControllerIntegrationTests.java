package com.example.crudsquel;


import com.example.crudsquel.DateFormat.LocalDateAdapter;
import com.example.crudsquel.hibernateImpl.Service.DepartmentsServiceHiber;
import com.example.crudsquel.jdbcImpl.Service.DepartmentsJdbcService;
import com.example.crudsquel.model.Departments;
import com.example.crudsquel.repository.DepartmentsRepository;
import com.example.crudsquel.service.DepartmentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DepartmentsControllerIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private DepartmentsRepository departmentsRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DepartmentsServiceHiber departmentsServiceHiber;

    @Autowired
    private DepartmentsJdbcService departmentsJdbcService;

    //    Utill dates
    private final int id = 80;
    private final Gson gson = new GsonBuilder().serializeNulls().registerTypeAdapter(LocalDate.class, new LocalDateAdapter()).create();

    // Spring data tests
    @Test
    public void testOnGetAllDepartments() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departments")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = departmentService.getAllDepartments(false).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetAllExpandDepartments() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departments?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = departmentService.getAllDepartments(true).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetOneDepartment() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departments/" + id)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String employeeFromRequest = mvcResult.getResponse().getContentAsString();
        String employeeFromDB = gson.toJson(departmentService.getDepartments(id, false));
        Assert.assertEquals(employeeFromRequest, employeeFromDB);
    }

    @Test
    public void testOnGetOneExpandDepartment() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departments/" + id + "?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String employeeFromRequest = mvcResult.getResponse().getContentAsString();
        String employeeFromDB = gson.toJson(departmentService.getDepartments(id, true));
        Assert.assertEquals(employeeFromRequest, employeeFromDB);
    }

    @Test
    public void testOnPutValidDepartment() throws Exception {
        Departments departments = new Departments();
        departments.setDepartmentName("Special Force");
        departments.setLocationId(1800);
//        These parameters aren`t going to set(When we`re going to update departments or job we must delete employees and insert)
        departments.setDepartmentId(departmentsRepository.findById(id).get().getDepartmentId());
        departments.setManagerId(departmentsRepository.findById(id).get().getManagerId());
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/api/departments/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(departments))
        ).andReturn();

        String employeeFromRequest = mvcResult.getRequest().getContentAsString();
        String employeeFromDb = gson.toJson(departmentService.getDepartments(id, false));
        Assert.assertEquals(employeeFromRequest, employeeFromDb);
    }

    //
    @Test
    public void testOnPostValidDepartment() throws Exception {
        Departments departments = new Departments();
        departments.setManagerId(111);
        departments.setDepartmentName("Help Resouces");
        departments.setLocationId(1800);
        departments.setDepartmentId(id);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/departments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(departments)))
                .andExpect(status().isOk()).andReturn();
        String departmentPosted = gson.toJson(departments);
        String departmentAdded = gson.toJson(departmentService.getDepartments(id, false));
        Assert.assertEquals(departmentPosted, departmentAdded);
    }

    @Test
    public void testOnDeleteDepartment() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/departments/" + id)).andReturn();
        Assert.assertTrue(departmentsRepository.findById(id).isEmpty());
    }

    // Hibernat tests
    @Test
    public void testOnGetAllDepartmentsHiber() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsHbr")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = departmentsServiceHiber.getAllDepartments(false).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetAllExpandDepartmentsHiber() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsHbr?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = departmentsServiceHiber.getAllDepartments(true).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetOneDepartmentHiber() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsHbr/" + id)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String employeeFromRequest = mvcResult.getResponse().getContentAsString();
        String employeeFromDB = gson.toJson(departmentsServiceHiber.getDepartment(id, false));
        Assert.assertEquals(employeeFromRequest, employeeFromDB);
    }

    @Test
    public void testOnGetOneExpandDepartmentHiber() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsHbr/" + id + "?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String employeeFromRequest = mvcResult.getResponse().getContentAsString();
        String employeeFromDB = gson.toJson(departmentsServiceHiber.getDepartment(id, true));
        Assert.assertEquals(employeeFromRequest, employeeFromDB);
    }

    @Test
    public void testOnPutValidDepartmentsHiber() throws Exception {
        Departments departments = new Departments();
        departments.setDepartmentName("Special Force");
        departments.setLocationId(1800);
//        These parameters aren`t going to set(When we`re going to update departments or job we must delete employees and insert)
        departments.setDepartmentId(departmentsRepository.findById(id).get().getDepartmentId());
        departments.setManagerId(departmentsRepository.findById(id).get().getManagerId());
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/api/departmentsHbr/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(departments))
        ).andReturn();

        String departmentFromRequest = mvcResult.getRequest().getContentAsString();
        String departmentFromDb = gson.toJson(departmentsServiceHiber.getDepartment(id, false));
        Assert.assertEquals(departmentFromRequest, departmentFromDb);
    }

    @Test
    public void testOnPostValidDepartmentsHiber() throws Exception {
        Departments departments = new Departments();
        departments.setManagerId(111);
        departments.setDepartmentName("Help Resouces");
        departments.setLocationId(1800);
        departments.setDepartmentId(140);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/departmentsHbr")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(departments)))
                .andExpect(status().isOk()).andReturn();
        String departmentPosted = gson.toJson(departments);
        String departmentAdded = gson.toJson(departmentsServiceHiber.getDepartment(140, false));
        Assert.assertEquals(departmentPosted, departmentAdded);
    }

    @Test
    public void testOnDeleteDepartmentsHiber() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/departmentsHbr/" + id)).andReturn();
        Assert.assertTrue(departmentsRepository.findById(id).isEmpty());
    }

    // Jdbc integration tests

    @Test
    public void testOnGetAllDepartmentsJdbc() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsJdbc")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = departmentsJdbcService.getAllDepartments(false).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetAllExpandDepartmentsJdbc() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsJdbc?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = departmentsJdbcService.getAllDepartments(true).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetOneDepartmentJdbc() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsJdbc/" + id)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String departmentsFromRequest = mvcResult.getResponse().getContentAsString();
        String departmentsFromDB = gson.toJson(departmentsJdbcService.getDepartmentsById(false, id));
        Assert.assertEquals(departmentsFromRequest, departmentsFromDB);
    }

    @Test
    public void testOnGetOneExpandDepartmentJdbc() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsJdbc/" + id + "?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String departmentsFromRequest = mvcResult.getResponse().getContentAsString();
        String departmentsFromDB = gson.toJson(departmentsJdbcService.getDepartmentsById(true, id));
        Assert.assertEquals(departmentsFromRequest, departmentsFromDB);
    }

    @Test
    public void testOnPutValidDepartmentsJdbc() throws Exception {
        Departments departments = new Departments();
        departments.setDepartmentName("Special Force");
        departments.setLocationId(1800);
//        These parameters aren`t going to set(When we`re going to update departments or job we must delete employees and insert)
        departments.setDepartmentId(departmentsRepository.findById(id).get().getDepartmentId());
        departments.setManagerId(departmentsRepository.findById(id).get().getManagerId());
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/api/departmentsJdbc/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(departments))
        ).andReturn();

        String departmentFromRequest = mvcResult.getRequest().getContentAsString();
        String departmentFromDb = gson.toJson(departmentsJdbcService.getDepartmentsById(false, id));
        Assert.assertEquals(departmentFromRequest, departmentFromDb);
    }

    @Test
    public void testOnPostValidDepartmentsJdbc() throws Exception {
        Departments departments = new Departments();
        departments.setManagerId(111);
        departments.setDepartmentName("Help Resouces");
        departments.setLocationId(1800);
        departments.setDepartmentId(290);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/departmentsJdbc")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(departments)))
                .andExpect(status().isOk()).andReturn();
        String departmentPosted = gson.toJson(departments);
        String departmentAdded = gson.toJson(departmentsJdbcService.getDepartmentsById(false, 290));
        Assert.assertEquals(departmentPosted, departmentAdded);
    }

    @Test
    public void testOnDeleteDepartmentsJdbc() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/departmentsJdbc/" + 270)).andExpect(status().isOk());
        Assert.assertTrue(departmentsRepository.findById(270).isEmpty());
    }

}
