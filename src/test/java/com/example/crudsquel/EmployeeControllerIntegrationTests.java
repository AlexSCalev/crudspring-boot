package com.example.crudsquel;


import com.example.crudsquel.DateFormat.LocalDateAdapter;
import com.example.crudsquel.hibernateImpl.Service.EmployeeServiceHiber;
import com.example.crudsquel.jdbcImpl.Service.EmployeeJdbcService;
import com.example.crudsquel.model.Employees;
import com.example.crudsquel.repository.EmployeesRepository;
import com.example.crudsquel.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeesRepository employeesRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private EmployeeServiceHiber employeeServiceHiber;

    @Autowired
    private EmployeeJdbcService employeeJdbcService;

    //    Utill dates
    private final int id = 102;
    private final Gson gson = new GsonBuilder().serializeNulls().registerTypeAdapter(LocalDate.class, new LocalDateAdapter()).create();
    private Employees employeeForTest;
    private final String email = "test@gmail.com";

    private void initEmployeesTest() {
        employeeForTest = new Employees();
        employeeForTest.setEmployeeId(id);
        employeeForTest.setFirstName("Test");
        employeeForTest.setLastName("Test");
        employeeForTest.setEmail(email);
        employeeForTest.setPhoneNumber("012345678");
        employeeForTest.setHireDate(LocalDate.of(2021, 4, 18));
        employeeForTest.setSalary(14000.0);
        employeeForTest.setCommissionPct(0.20);
        employeeForTest.setManagerId(null);
//        These parameters aren`t going to set(When we`re going to update departments or job we must delete employees and insert)
        employeeForTest.setJobId(Objects.requireNonNull(employeesRepository.findById(id).orElse(null)).getJobId());
        employeeForTest.setDepartmentId(Objects.requireNonNull(employeesRepository.findById(id).orElse(null)).getDepartmentId());
    }

    // Spring data tests
    @Test
    public void testOnGetAllEmployeees() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employees")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = employeeService.getAllEmpoyees(false).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetAllExpandEmployees() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employees?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = employeeService.getAllEmpoyees(true).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetOneEmployees() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employees/" + id)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String employeeFromRequest = mvcResult.getResponse().getContentAsString();
        String employeeFromDB = gson.toJson(employeeService.getEmployee(id, false));
        Assert.assertEquals(employeeFromRequest, employeeFromDB);
    }

    @Test
    public void testOnGetOneExpandEmployees() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employees/" + id + "?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String employeeFromRequest = mvcResult.getResponse().getContentAsString();
        String employeeFromDB = gson.toJson(employeeService.getEmployee(id, true));
        Assert.assertEquals(employeeFromRequest, employeeFromDB);
    }

    @Test
    public void testOnPutValidEmployees() throws Exception {
        initEmployeesTest();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/api/employees/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employeeForTest))
        ).andReturn();
        String employeeFromRequest = mvcResult.getRequest().getContentAsString();
        String employeeFromDb = gson.toJson(employeeService.getEmployee(id, false));
        Assert.assertEquals(employeeFromRequest, employeeFromDb);
    }

    @Test
    public void testOnPostValidEmployees() throws Exception {
        initEmployeesTest();
        mockMvc.perform(MockMvcRequestBuilders.post("/api/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employeeForTest)))
                .andExpect(status().isOk()).andReturn();

        Employees empFromDB = employeesRepository.getEmployeesByEmail(email);
        String findFromDB = gson.toJson(empFromDB);
        employeeForTest.setEmployeeId(empFromDB.getEmployeeId());
        String addedEmployee = gson.toJson(employeeForTest);
        Assert.assertEquals(addedEmployee, findFromDB);
    }

    @Test
    public void testOnDeleteEmployee() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/employees/" + 200)).andReturn();
        Assert.assertTrue(employeesRepository.findById(200).isEmpty());
    }

    // Hibernat tests

    @Test
    public void testOnGetAllEmployeeesHiber() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesHiber")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = employeeServiceHiber.getAllEmployees(false).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetAllExpandEmployeesHiber() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesHiber?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = employeeServiceHiber.getAllEmployees(true).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetOneEmployeesHiber() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesHiber/" + id)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String employeeFromRequest = mvcResult.getResponse().getContentAsString();
        String employeeFromDB = gson.toJson(employeeServiceHiber.getEmployeeById(id, false));
        Assert.assertEquals(employeeFromRequest, employeeFromDB);
    }

    @Test
    public void testOnGetOneExpandEmployeesHiber() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesHiber/" + id + "?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String employeeFromRequest = mvcResult.getResponse().getContentAsString();
        String employeeFromDB = gson.toJson(employeeServiceHiber.getEmployeeById(id, true));
        Assert.assertEquals(employeeFromRequest, employeeFromDB);
    }

    @Test
    public void testOnPutValidEmployeesHiber() throws Exception {
        initEmployeesTest();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/api/employeesHiber/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employeeForTest))
        ).andReturn();

        String employeeFromRequest = mvcResult.getRequest().getContentAsString();
        String employeeFromDb = gson.toJson(employeeServiceHiber.getEmployeeById(id, false));
        Assert.assertEquals(employeeFromRequest, employeeFromDb);
    }

    @Test
    public void testOnPostValidEmployeesHiber() throws Exception {
        initEmployeesTest();
        mockMvc.perform(MockMvcRequestBuilders.post("/api/employeesHiber")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employeeForTest)))
                .andExpect(status().isOk()).andReturn();

        Employees empFromDB = employeesRepository.getEmployeesByEmail(email);
        String findFromDB = gson.toJson(empFromDB);
        employeeForTest.setEmployeeId(empFromDB.getEmployeeId());
        String addedEmplyees = gson.toJson(employeeForTest);
        Assert.assertEquals(addedEmplyees, findFromDB);
    }

    @Test
    public void testOnDeleteEmployeeHiber() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/employeesHiber/" + 62)).andReturn();
        Assert.assertTrue(employeesRepository.findById(62).isEmpty());
    }


    // JDBC realization

    @Test
    public void testOnGetAllEmployeesJdbc() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesJdbc")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = employeeJdbcService.getAllEmployees(false).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetAllExpandEmployeesJdbc() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesJdbc?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String listInOneString = mvcResult.getResponse().getContentAsString().replaceAll("\\s", "");
        String listFromDb = employeeJdbcService.getAllEmployees(true).stream()
                .map(gson::toJson)
                .collect(Collectors.toList()).toString().replaceAll("\\s", "");
        Assert.assertEquals(listInOneString, listFromDb);
    }

    @Test
    public void testOnGetOneEmployeesJdbc() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesJdbc/" + id)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String employeesFromRequest = mvcResult.getResponse().getContentAsString();
        String employeesFromDB = gson.toJson(employeeJdbcService.getEmployeeById(false, id));
        Assert.assertEquals(employeesFromRequest, employeesFromDB);
    }

    @Test
    public void testOnGetOneExpandEmployeesJdbc() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesJdbc/" + id + "?expand=true")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String departmentsFromRequest = mvcResult.getResponse().getContentAsString();
        String departmentsFromDB = gson.toJson(employeeJdbcService.getEmployeeById(true, id));
        Assert.assertEquals(departmentsFromRequest, departmentsFromDB);
    }

    @Test
    public void testOnPutValidEmployeesJdbc() throws Exception {
        initEmployeesTest();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/api/employeesJdbc/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employeeForTest))
        ).andReturn();

        String employeesFromRequest = mvcResult.getRequest().getContentAsString();
        String employeesFromDb = gson.toJson(employeeJdbcService.getEmployeeById(false, id));
        Assert.assertEquals(employeesFromRequest, employeesFromDb);
    }

    @Test
    public void testOnPostValidEmployeesJdbc() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/api/employeesJdbc")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employeeForTest)))
                .andExpect(status().isOk()).andReturn();

        Employees empFromDB = employeesRepository.getEmployeesByEmail(email);
        String findFromDB = gson.toJson(empFromDB);
        employeeForTest.setEmployeeId(empFromDB.getEmployeeId());
        String addedEmplyees = gson.toJson(employeeForTest);
        Assert.assertEquals(addedEmplyees, findFromDB);
    }

    @Test
    public void testOnDeleteDepartmentsJdbc() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/employeesJdbc/" + id)).andExpect(status().isOk());
        Assert.assertTrue(employeesRepository.findById(id).isEmpty());
    }

}
