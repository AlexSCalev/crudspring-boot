package com.example.crudsquel;

import com.example.crudsquel.hibernateImpl.Service.EmployeeServiceHiber;
import com.example.crudsquel.jdbcImpl.Service.EmployeeJdbcService;
import com.example.crudsquel.model.Employees;
import com.example.crudsquel.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeesControllerUnitTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    EmployeeService employeeService;

    @MockBean
    EmployeeServiceHiber employeeServiceHiber;

    @MockBean
    EmployeeJdbcService employeeJdbcService;

    private final Employees emp = new Employees();
    private final ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

    private void initEmployees(){
        emp.setEmployeeId(33);
        emp.setFirstName("Harry");
        emp.setLastName("Potter");
        emp.setEmail("harry.potter@gmail.com");
        emp.setPhoneNumber("012345678");
        emp.setHireDate(null);
        emp.setJobId("AD_VP");
        emp.setSalary(17000.0);
        emp.setCommissionPct(null);
    }

// Spring data tests from Employees Controllers

    @Test
    public void testOnCallGetRequestAllEmployeesWithJpaSevice() throws Exception {
        when(employeeService.getAllEmpoyees(false)).thenReturn(Collections.emptyList());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees").accept(MediaType.APPLICATION_JSON)).andReturn();
        verify(employeeService).getAllEmpoyees(false);
    }

    @Test
    public void testOnCallGetRequestOneEmployeeWithJpaSevice() throws Exception {
        when(employeeService.getEmployee(1,false)).thenReturn(new Employees());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employees/1")).andReturn();
        verify(employeeService).getEmployee(1,false);
    }

    @Test
    public void testOnCallPutRequestEmployeeWithJpaService() throws Exception {
        initEmployees();
        doNothing().when(employeeService).updateEmployee(Mockito.any(),Mockito.any());
        mockMvc.perform(MockMvcRequestBuilders.put("/api/employees/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(emp)))
                .andExpect(status().isOk());
        verify(employeeService).updateEmployee(Mockito.any(),Mockito.any());
    }

    @Test
    public void testOnCallPostRequestEmployeeWithJpaService() throws Exception {
        initEmployees();
        doNothing().when(employeeService).addEmployee(Mockito.any());
        mockMvc.perform(MockMvcRequestBuilders.post("/api/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(emp)))
                .andExpect(status().isOk());
        verify(employeeService).addEmployee(Mockito.any());
    }

    @Test
    public void testOnCallDeleteRequestWithJpaService() throws Exception {
        doNothing().when(employeeService).deleteEmployee(1);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/employees/1"))
                .andExpect(status().isOk());
        verify(employeeService).deleteEmployee(1);
    }

// Hibernate Tests from EmployeesControllers

    @Test
    public void testOnCallGetAllRequestWithHibernate() throws Exception {
        when(employeeServiceHiber.getAllEmployees(false)).thenReturn(Collections.emptyList());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesHiber")).andExpect(status().isOk());
        verify(employeeServiceHiber).getAllEmployees(false);
    }

    @Test
    public void testOnCallGetOneRequestWithHibernate() throws Exception {
        when(employeeServiceHiber.getEmployeeById(1,false)).thenReturn(new Employees());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesHiber/1")).andExpect(status().isOk());
        verify(employeeServiceHiber).getEmployeeById(1,false);
    }

    @Test
    public void testOnCallPostRequestWithHibernate() throws Exception {
        initEmployees();
        doNothing().when(employeeServiceHiber).addEmployees(Mockito.any());
        mockMvc.perform(MockMvcRequestBuilders.post("/api/employeesHiber")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(emp)))
                .andExpect(status().isOk());
        verify(employeeServiceHiber).addEmployees(Mockito.any());
    }

    @Test
    public void testOnCallPutRequestWithHibernate() throws Exception {
        initEmployees();
        doNothing().when(employeeServiceHiber).updateEmployee(Mockito.any(),Mockito.any());
        mockMvc.perform(MockMvcRequestBuilders.put("/api/employeesHiber/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(emp)))
                .andExpect(status().isOk());
        verify(employeeServiceHiber).updateEmployee(Mockito.any(),Mockito.any());
    }

    @Test
    public void testOnCallDeleteRequestWithHibernate() throws Exception{
        doNothing().when(employeeServiceHiber).deleteEmployee(Mockito.any());
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/employeesHiber/1")).andExpect(status().isOk());
        verify(employeeServiceHiber).deleteEmployee(Mockito.any());
    }

//    JDBC Unit test on call methods

    @Test
    public void testOnCallGetAllRequestWithJdbc() throws Exception {
        when(employeeJdbcService.getAllEmployees(anyBoolean())).thenReturn(Collections.emptyList());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesJdbc")).andExpect(status().isOk());
        verify(employeeJdbcService).getAllEmployees(anyBoolean());
    }

    @Test
    public void testOnCallGetOneRequestWithJdbc() throws Exception {
        when(employeeJdbcService.getEmployeeById(anyBoolean(),anyInt())).thenReturn(new Employees());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/employeesJdbc/1")).andExpect(status().isOk());
        verify(employeeJdbcService).getEmployeeById(anyBoolean(),anyInt());
    }

    @Test
    public void testOnCallPutRequestWithJdbc() throws Exception {
        initEmployees();

        doNothing().when(employeeJdbcService).updateEmployee(any(),any());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/employeesJdbc/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(emp)))
                .andExpect(status().isOk());

        verify(employeeJdbcService).updateEmployee(any(),any());
    }

    @Test
    public void testOnCallPostRequestWithJdbc() throws Exception {
        initEmployees();
        doNothing().when(employeeJdbcService).addEmplyee(any());
        mockMvc.perform(MockMvcRequestBuilders.post("/api/employeesJdbc")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(emp)))
                .andExpect(status().isOk());
        verify(employeeJdbcService).addEmplyee(any());
    }

    @Test
    public void testOnCallDeleteRequestWithJdbc() throws Exception {
        initEmployees();
        doNothing().when(employeeJdbcService).deleteEmplyee(any());
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/employeesJdbc/1")).andExpect(status().isOk());
        verify(employeeJdbcService).deleteEmplyee(any());
    }



}
