package com.example.crudsquel;

import com.example.crudsquel.hibernateImpl.Service.DepartmentsServiceHiber;
import com.example.crudsquel.jdbcImpl.Service.DepartmentsJdbcService;
import com.example.crudsquel.model.Departments;
import com.example.crudsquel.service.DepartmentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DepartmentsControllerUnitTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    DepartmentService departmentService;

    @MockBean
    DepartmentsServiceHiber departmentsServiceHiber;

    @MockBean
    DepartmentsJdbcService departmentsJdbcService;


    private final Departments departmentsTest = new Departments();
    private final ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

    private void initdepartmentsTest() {
        departmentsTest.setManagerId(101);
        departmentsTest.setDepartmentId(80);
        departmentsTest.setDepartmentName("Human Resources Advanced");
        departmentsTest.setLocationId(2300);
    }

    @Test
    public void testRequestGetAllDepartmentsDepartmentsService() throws Exception {
        when(departmentService.getAllDepartments(false)).thenReturn(Collections.emptyList());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/departments")).andExpect(status().isOk());
        verify(departmentService).getAllDepartments(false);
    }

    @Test
    public void testRequestGetOneDepartmentDepartmentsService() throws Exception {
        when(departmentService.getDepartments(1, false)).thenReturn(new Departments());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/departments/1")).andExpect(status().isOk());
        verify(departmentService).getDepartments(1, false);
    }

    @Test
    public void testRequestPutDepartmentDepartmentsService() throws Exception {
        initdepartmentsTest();
        doNothing().when(departmentService).updateDepartment(any(), any());
        mockMvc.perform(MockMvcRequestBuilders.put("/api/departments/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(departmentsTest)))
                .andExpect(status().isOk());
        verify(departmentService).updateDepartment(any(), any());
    }

    @Test
    public void testRequestPostDepartmentDepartmentsService() throws Exception {
        initdepartmentsTest();
        doNothing().when(departmentService).addDepartment(any());
        mockMvc.perform(MockMvcRequestBuilders.post("/api/departments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(departmentsTest)))
                .andExpect(status().isOk());
        verify(departmentService).addDepartment(any());
    }

    @Test
    public void testRequestDeleteDeparemtnsDepartmentsService() throws Exception {
        doNothing().when(departmentService).removeDepartment(1);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/departments/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(departmentService).removeDepartment(1);
    }


//    Hiber test for departments controllers

    @Test
    public void testRequestGetAllDepartmentsDepartmentsHiber() throws Exception {
        when(departmentsServiceHiber.getAllDepartments(false)).thenReturn(Collections.emptyList());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsHbr")).andExpect(status().isOk());
        verify(departmentsServiceHiber).getAllDepartments(false);
    }

    @Test
    public void testRequestGetOneDepartmentDepartmentsHiber() throws Exception {
        when(departmentsServiceHiber.getDepartment(1, false)).thenReturn(new Departments());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsHbr/1")).andExpect(status().isOk());
        verify(departmentsServiceHiber).getDepartment(1, false);
    }

    @Test
    public void testRequestPutDepartmentDepartmentsHiber() throws Exception {
        initdepartmentsTest();

        doNothing().when(departmentsServiceHiber).updateDepartments(any(), any());
        mockMvc.perform(MockMvcRequestBuilders.put("/api/departmentsHbr/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(departmentsTest)))
                .andExpect(status().isOk());
        verify(departmentsServiceHiber).updateDepartments(any(), any());
    }

    @Test
    public void testRequestPostDepartmentDepartmentsHiber() throws Exception {
        initdepartmentsTest();
        doNothing().when(departmentsServiceHiber).addDepartment(any());
        mockMvc.perform(MockMvcRequestBuilders.post("/api/departmentsHbr")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(departmentsTest)))
                .andExpect(status().isOk());
        verify(departmentsServiceHiber).addDepartment(any());
    }

    @Test
    public void testRequestDeleteDeparemtnsDepartmentsHiber() throws Exception {
        doNothing().when(departmentsServiceHiber).deleteDepartments(1);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/departmentsHbr/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(departmentsServiceHiber).deleteDepartments(1);
    }

    // JDBC Unit tests

    @Test
    public void testRequestGetAllDepartmentsJdbc() throws Exception {
        when(departmentsJdbcService.getAllDepartments(anyBoolean())).thenReturn(Collections.emptyList());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsJdbc")).andExpect(status().isOk());
        verify(departmentsJdbcService).getAllDepartments(anyBoolean());
    }

    @Test
    public void testRequestGetOneDepartmentJdbc() throws Exception {
        when(departmentsJdbcService.getDepartmentsById(anyBoolean(), anyInt())).thenReturn(new Departments());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/departmentsJdbc/1")).andExpect(status().isOk());
        verify(departmentsJdbcService).getDepartmentsById(anyBoolean(), anyInt());
    }

    @Test
    public void testRequestPutDepartmentJdbc() throws Exception {
        initdepartmentsTest();
        doNothing().when(departmentsJdbcService).updateDepartments(anyInt(), any());
        mockMvc.perform(MockMvcRequestBuilders.put("/api/departmentsJdbc/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(departmentsTest)))
                .andExpect(status().isOk());
        verify(departmentsJdbcService).updateDepartments(anyInt(), any());
    }

    @Test
    public void testRequestPostDepartmentJdbc() throws Exception {
        initdepartmentsTest();
        doNothing().when(departmentsJdbcService).addDepartment(any());
        mockMvc.perform(MockMvcRequestBuilders.post("/api/departmentsJdbc")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(departmentsTest)))
                .andExpect(status().isOk());
        verify(departmentsJdbcService).addDepartment(any());
    }

    @Test
    public void testRequestDeleteDeparemtnsJdbc() throws Exception {
        doNothing().when(departmentsJdbcService).deleteDepartment(any());
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/departmentsJdbc/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(departmentsJdbcService).deleteDepartment(any());
    }


}
