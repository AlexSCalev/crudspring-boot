package com.example.crudsquel.repository;

import com.example.crudsquel.model.JobHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface JobHistoryRepository extends JpaRepository<JobHistory, Integer> {

    @Transactional
    @Modifying
    @Query("DELETE FROM JobHistory WHERE employeeId =:empId")
    void deleteAllByEmployeesId(@Param("empId") Integer empid);

    @Transactional
    @Modifying
    @Query("UPDATE JobHistory SET departmentId = null where departmentId = :dempId")
    void removeAllDepartmentRelation(@Param("dempId") Integer dempId);

    @Transactional
    @Modifying
    @Query("DELETE FROM JobHistory where employeeId in (select employeeId from Employees WHERE departmentId = :dempId)")
    void removeExistingHistory(@Param("dempId") Integer dempId);
}
