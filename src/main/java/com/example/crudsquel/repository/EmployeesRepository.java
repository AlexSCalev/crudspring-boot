package com.example.crudsquel.repository;

import com.example.crudsquel.model.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
@Repository
public interface EmployeesRepository extends JpaRepository<Employees, Integer> {


    Employees getEmployeesByEmail(String email);

    @Transactional
    @Modifying
    @Query("UPDATE Employees SET managerId = null WHERE managerId = :managerId")
    void setNullManagerId(@Param("managerId") Integer id);

    @Transactional
    @Modifying
    @Query("UPDATE Employees SET departmentId = null WHERE departmentId = :departmentId")
    void setNullDepartmentIdOnDelete(@Param("departmentId") Integer id);
}
