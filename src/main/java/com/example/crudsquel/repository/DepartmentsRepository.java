package com.example.crudsquel.repository;

import com.example.crudsquel.model.Departments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DepartmentsRepository extends JpaRepository<Departments,Integer> {

    @Transactional
    @Modifying
    @Query("UPDATE Departments SET managerId = null where managerId = :managerId")
    void setNullableManager(@Param("managerId") Integer id);

}
