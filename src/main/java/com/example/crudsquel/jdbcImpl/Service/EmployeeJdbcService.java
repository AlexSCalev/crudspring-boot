package com.example.crudsquel.jdbcImpl.Service;

import com.example.crudsquel.Interfaces.EmployeesCommon;
import com.example.crudsquel.jdbcImpl.Repository.EmployeesDAORepository;
import com.example.crudsquel.model.EmployeeShort;
import com.example.crudsquel.model.Employees;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeJdbcService {

    private final EmployeesDAORepository employeesDAORepository;

    public EmployeeJdbcService(JdbcTemplate jdbcTemplate) {
        this.employeesDAORepository = new EmployeesDAORepository(jdbcTemplate);
    }

    private EmployeeShort convertToShort(Employees employees) {
        return new EmployeeShort(employees.getFirstName(), employees.getLastName(), employees.getDepartmentId());
    }

    public List<EmployeesCommon> getAllEmployees(boolean expand) {
        return (expand) ? employeesDAORepository.getListOfAllEmployees().stream().map(this::convertToShort).collect(Collectors.toList()) : new ArrayList<>(employeesDAORepository.getListOfAllEmployees());
    }

    public EmployeesCommon getEmployeeById(boolean expand, Integer id) {
        Employees employees = employeesDAORepository.getEmplyeeById(id);
        return (expand) ? convertToShort(employees) : employees;
    }

    public void updateEmployee(Integer id, Employees employees) {
        employeesDAORepository.updateEmployeeByID(employees, id);
    }

    public void addEmplyee(Employees employee) {
        employeesDAORepository.save(employee);
    }

    public void deleteEmplyee(Integer id) {
        employeesDAORepository.deleteEmployee(id);
    }
}
