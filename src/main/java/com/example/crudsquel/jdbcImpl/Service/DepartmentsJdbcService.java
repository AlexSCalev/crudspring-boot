package com.example.crudsquel.jdbcImpl.Service;

import com.example.crudsquel.Interfaces.DepartmentsCommon;
import com.example.crudsquel.jdbcImpl.Repository.DepartmentsDAORepository;
import com.example.crudsquel.model.Departments;
import com.example.crudsquel.model.DepartmentsShort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentsJdbcService {

    private final DepartmentsDAORepository departmentsDAORepository;

    public DepartmentsJdbcService(JdbcTemplate jdbcTemplate) {
        this.departmentsDAORepository = new DepartmentsDAORepository(jdbcTemplate);
    }


    private DepartmentsShort convertToShort(Departments departments) {
        return new DepartmentsShort(departments.getDepartmentName());
    }


    public List<DepartmentsCommon> getAllDepartments(boolean expand) {
        return (expand) ? departmentsDAORepository.getListOfAllDepartments().stream().map(this::convertToShort).collect(Collectors.toList()) : new ArrayList<>(departmentsDAORepository.getListOfAllDepartments());
    }

    public DepartmentsCommon getDepartmentsById(boolean expand, Integer id) {
        Departments departments = departmentsDAORepository.getDepartmentsById(id);
        return (expand) ? convertToShort(departments) : departments;
    }

    public void updateDepartments(Integer id, Departments departments) {
        departmentsDAORepository.updateDepartmentsByID(departments, id);
    }

    public void addDepartment(Departments departments) {
        departmentsDAORepository.save(departments);
    }

    public void deleteDepartment(Integer id) {
        departmentsDAORepository.deleteDepartments(id);
    }

}
