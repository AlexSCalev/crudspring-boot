package com.example.crudsquel.jdbcImpl.Repository;

import com.example.crudsquel.model.Employees;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
public class EmployeesDAORepository {

    private final JdbcTemplate jdbcTemplate;

    public EmployeesDAORepository(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Employees> getListOfAllEmployees(){
       return jdbcTemplate.query("SELECT * FROM Employees", BeanPropertyRowMapper.newInstance(Employees.class));
    }

    public Employees getEmplyeeById(Integer id){
        String query = "SELECT * FROM Employees WHERE employee_id = ?";
        return jdbcTemplate.queryForObject(query,BeanPropertyRowMapper.newInstance(Employees.class),id);
    }

    public void save(Employees employees) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        simpleJdbcInsert.withTableName("employees").usingColumns(
                "employee_id","first_name","last_name",
                "email","phone_number","hire_date"
                ,"job_id","salary","commission_pct","manager_id","department_id");
        BeanPropertySqlParameterSource param = new BeanPropertySqlParameterSource(employees);
        simpleJdbcInsert.execute(param);
    }

    @Transactional
    public void updateEmployeeByID(Employees employees,Integer id) {
        jdbcTemplate.update(
                "UPDATE EMPLOYEES SET first_name = ?,last_name = ?, email = ?, " +
                "phone_number = ? ,hire_date = ?, salary = ?, " +
                "commission_pct = ? , manager_id = ? " +
                " WHERE EMPLOYEE_ID = ?",
                employees.getFirstName(), employees.getLastName() , employees.getEmail(), employees.getPhoneNumber() ,
                employees.getHireDate() , employees.getSalary() , employees.getCommissionPct() ,
                employees.getManagerId() , id);
    }

    @Transactional
    public void deleteEmployee(Integer id){
        jdbcTemplate.update("DELETE FROM JOB_HISTORY WHERE EMPLOYEE_ID = ?", id);
        jdbcTemplate.update("UPDATE Employees SET MANAGER_ID = null WHERE MANAGER_ID = ?", id);
        jdbcTemplate.update("UPDATE Departments SET MANAGER_ID = null where MANAGER_ID =?",id);
        jdbcTemplate.update("DELETE EMPLOYEES where EMPLOYEE_ID = ?", id);
    }
}
