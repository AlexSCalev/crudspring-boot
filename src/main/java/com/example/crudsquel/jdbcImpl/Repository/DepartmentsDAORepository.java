package com.example.crudsquel.jdbcImpl.Repository;

import com.example.crudsquel.model.Departments;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class DepartmentsDAORepository {
    private final JdbcTemplate jdbcTemplate;

    public DepartmentsDAORepository(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Departments> getListOfAllDepartments(){
        return jdbcTemplate.query("SELECT * FROM DEPARTMENTS", BeanPropertyRowMapper.newInstance(Departments.class));
    }

    public Departments getDepartmentsById(Integer id){
        String query = "SELECT * FROM DEPARTMENTS WHERE DEPARTMENT_ID = ?";
        return jdbcTemplate.queryForObject(query, BeanPropertyRowMapper.newInstance(Departments.class), id);
    }

    public void save(Departments departments) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        simpleJdbcInsert.withTableName("departments").usingColumns(
                "department_id","department_name","manager_id","location_id");
        BeanPropertySqlParameterSource param = new BeanPropertySqlParameterSource(departments);
        simpleJdbcInsert.execute(param);
    }

    @Transactional
    public void updateDepartmentsByID(Departments departments,Integer id) {
        jdbcTemplate.update(
                "UPDATE DEPARTMENTS SET DEPARTMENT_ID = ?, DEPARTMENT_NAME = ?,MANAGER_ID = ? , LOCATION_ID = ? WHERE DEPARTMENT_ID = ?",
                departments.getDepartmentId(),departments.getDepartmentName() ,departments.getManagerId(),departments.getLocationId(),
                id);
    }

    @Transactional
    public void deleteDepartments(Integer id){
        jdbcTemplate.update("DELETE FROM JOB_HISTORY where EMPLOYEE_ID in (select EMPLOYEES.EMPLOYEE_ID from Employees WHERE EMPLOYEES.DEPARTMENT_ID = ?)", id);
        jdbcTemplate.update("UPDATE Employees SET DEPARTMENT_ID = null WHERE DEPARTMENT_ID = ?", id);
        jdbcTemplate.update("UPDATE JOB_HISTORY SET DEPARTMENT_ID = null where DEPARTMENT_ID = ?", id);
        jdbcTemplate.update("DELETE DEPARTMENTS where DEPARTMENT_ID = ?",id);
    }
}
