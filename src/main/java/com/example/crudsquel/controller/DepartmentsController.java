package com.example.crudsquel.controller;

import com.example.crudsquel.Interfaces.DepartmentsCommon;
import com.example.crudsquel.hibernateImpl.Service.DepartmentsServiceHiber;
import com.example.crudsquel.jdbcImpl.Service.DepartmentsJdbcService;
import com.example.crudsquel.model.Departments;
import com.example.crudsquel.service.DepartmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class DepartmentsController {

    private final DepartmentService departmentService;
    private final DepartmentsServiceHiber departmentServiceHiber;
    private final DepartmentsJdbcService departmentsJdbcService;

    public DepartmentsController(DepartmentsJdbcService departmentsJdbcService, DepartmentService departmentService, DepartmentsServiceHiber departmentServiceHiber) {
        this.departmentService = departmentService;
        this.departmentServiceHiber = departmentServiceHiber;
        this.departmentsJdbcService = departmentsJdbcService;
    }

    //    JDBC Implimentation
    @GetMapping(value = "/departmentsJdbc")
    public List<DepartmentsCommon> getAllDepartmentsJdbc(@RequestParam(required = false, defaultValue = "false") boolean expand) {
        return departmentsJdbcService.getAllDepartments(expand);
    }

    @GetMapping(value = "/departmentsJdbc/{id}")
    public DepartmentsCommon getDepartmentJdbc(@PathVariable Integer id, @RequestParam(required = false, defaultValue = "false") boolean expand) {
        return departmentsJdbcService.getDepartmentsById(expand, id);
    }

    @PutMapping(value = "/departmentsJdbc/{id}")
    public ResponseEntity<String> updateDepartmentJdbc(@PathVariable Integer id, @RequestBody Departments departments) {
        departmentsJdbcService.updateDepartments(id, departments);
        return new ResponseEntity<>("Departments was updated succesfull", HttpStatus.OK);
    }

    @PostMapping("/departmentsJdbc")
    public ResponseEntity<String> addDepartmentJdbc(@RequestBody Departments departments) {
        departmentsJdbcService.addDepartment(departments);
        return new ResponseEntity<>("Departments was added successfully", HttpStatus.OK);
    }

    @DeleteMapping("/departmentsJdbc/{id}")
    public ResponseEntity<String> deleteDepartmentJdbc(@PathVariable Integer id) {
        departmentsJdbcService.deleteDepartment(id);
        return new ResponseEntity<>("Departments was deleted successfully", HttpStatus.OK);
    }


    //    Hibernate Implimentation
    @GetMapping(value = "/departmentsHbr")
    public List<DepartmentsCommon> printAllDepartmentsHiber(@RequestParam(required = false, defaultValue = "false") String expand) {
        return departmentServiceHiber.getAllDepartments(Boolean.parseBoolean(expand));
    }

    @GetMapping(value = "/departmentsHbr/{id}")
    public DepartmentsCommon printDepartmentHiber(@PathVariable Integer id, @RequestParam(required = false, defaultValue = "false") String expand) {
        return departmentServiceHiber.getDepartment(id, Boolean.parseBoolean(expand));
    }

    @PostMapping(value = "/departmentsHbr")
    public ResponseEntity<String> addDepartment(@Validated @RequestBody Departments departments) {
        departmentServiceHiber.addDepartment(departments);
        return new ResponseEntity<>(departments.toString(), HttpStatus.OK);
    }

    @PutMapping(value = "/departmentsHbr/{id}")
    public ResponseEntity<String> updateDepartmentHbr(@PathVariable Integer id, @RequestBody Departments departments) {
        departmentServiceHiber.updateDepartments(id, departments);
        return new ResponseEntity<>(departments.toString(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/departmentsHbr/{id}")
    public ResponseEntity<Integer> deleteDepartments(@PathVariable Integer id) {
        departmentServiceHiber.deleteDepartments(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }


    //    Spring Data
    @GetMapping(value = "/departments")
    public List<DepartmentsCommon> printAllDepartments(@RequestParam(required = false, defaultValue = "false") String expand) {
        return departmentService.getAllDepartments(Boolean.parseBoolean(expand));
    }

    @GetMapping(value = "/departments/{id}")
    public DepartmentsCommon printDepartment(@PathVariable Integer id, @RequestParam(required = false, defaultValue = "false") String expand) {
        return departmentService.getDepartments(id, Boolean.parseBoolean(expand));
    }

    @PostMapping("/departments")
    public ResponseEntity<String> addDepartament(@Validated @RequestBody Departments departments) {
        departmentService.addDepartment(departments);
        return new ResponseEntity<>(departments.toString(), HttpStatus.OK);
    }

    @PutMapping(value = "/departments/{id}")
    public ResponseEntity<String> updateDepartment(@PathVariable Integer id, @Validated @RequestBody Departments departments) {
        departmentService.updateDepartment(id, departments);
        return new ResponseEntity<>(departments.toString(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/departments/{id}")
    public ResponseEntity<Integer> deleteDepartment(@PathVariable Integer id) {
        departmentService.removeDepartment(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }


}
