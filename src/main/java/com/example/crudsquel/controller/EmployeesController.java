package com.example.crudsquel.controller;

import com.example.crudsquel.Interfaces.EmployeesCommon;
import com.example.crudsquel.hibernateImpl.Service.EmployeeServiceHiber;
import com.example.crudsquel.jdbcImpl.Service.EmployeeJdbcService;
import com.example.crudsquel.model.Employees;
import com.example.crudsquel.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeesController {

    private final EmployeeService employeeService;
    private final EmployeeServiceHiber employeeServiceHiber;
    private final EmployeeJdbcService employeeJdbcService;

    public EmployeesController(EmployeeJdbcService employeeJdbcService, EmployeeService employeeService, EmployeeServiceHiber employeeServiceHiber) {
        this.employeeService = employeeService;
        this.employeeServiceHiber = employeeServiceHiber;
        this.employeeJdbcService = employeeJdbcService;
    }


    //    JDBC Realized
    @GetMapping(value = "/employeesJdbc")
    public List<EmployeesCommon> printAllEmployeesJdbc(@RequestParam(required = false, defaultValue = "false") String expand) {
        return employeeJdbcService.getAllEmployees(Boolean.parseBoolean(expand));
    }

    @GetMapping(value = "/employeesJdbc/{id}")
    public EmployeesCommon printOneEmployeeJdbc(@PathVariable Integer id , @RequestParam(required = false, defaultValue = "false") boolean expand){
        return employeeJdbcService.getEmployeeById(expand,id);
    }

    @PostMapping(value = "/employeesJdbc")
    public ResponseEntity<String> addEmployeeJdbc(@RequestBody Employees employees){
        employeeJdbcService.addEmplyee(employees);
        return new ResponseEntity<>("Employees was added successfully",HttpStatus.OK);
    }

    @PutMapping(value = "/employeesJdbc/{id}")
    public ResponseEntity<String> updateEmployeeJdbc(@PathVariable Integer id , @RequestBody Employees employees){
        employeeJdbcService.updateEmployee(id,employees);
        return new ResponseEntity<>("Employee was updated successfully",HttpStatus.OK);
    }

    @DeleteMapping(value = "/employeesJdbc/{id}")
    public ResponseEntity<String> deleteEmployeesJdbc(@PathVariable Integer id){
        employeeJdbcService.deleteEmplyee(id);
        return new ResponseEntity<>("Employee was deleted successfully" , HttpStatus.OK);
    }


    //    Hibernate Realize
    @GetMapping(value = "/employeesHiber")
    public List<EmployeesCommon> printAllEmployeesHiber(@RequestParam(required = false, defaultValue = "false") String expand){
        return employeeServiceHiber.getAllEmployees(Boolean.parseBoolean(expand));
    }

    @GetMapping(value = "/employeesHiber/{id}")
    public EmployeesCommon printEmployeeHiber(@PathVariable Integer id,@RequestParam(required = false, defaultValue = "false") String expand){
        return employeeServiceHiber.getEmployeeById(id,Boolean.parseBoolean(expand));
    }

    @PostMapping(value = "/employeesHiber")
    public ResponseEntity<String> addEmplyee(@RequestBody Employees employees) {
        employeeServiceHiber.addEmployees(employees);
        return new ResponseEntity<>("Employees was added succesful",HttpStatus.OK);
    }

    @PutMapping(value = "/employeesHiber/{id}")
    public ResponseEntity<String> addEmplyee(@RequestBody Employees employees , @PathVariable Integer id) {
        employeeServiceHiber.updateEmployee(employees,id);
        return new ResponseEntity<>("Employees was update succesful",HttpStatus.OK);
    }

    @DeleteMapping(value = "/employeesHiber/{id}")
    public ResponseEntity<String> deleteEmplyee(@PathVariable Integer id) {
        employeeServiceHiber.deleteEmployee(id);
        return new ResponseEntity<>("Employees was delete succesful",HttpStatus.OK);
    }

// Spring Data realize
    @GetMapping(value = "/employees")
    public List<EmployeesCommon> printAllEmployees(@RequestParam(required = false, defaultValue = "false") String expand){
        return employeeService.getAllEmpoyees(Boolean.parseBoolean(expand));
    }

    @GetMapping(value = "/employees/{id}")
    public EmployeesCommon printEmployee(@PathVariable Integer id , @RequestParam(required = false, defaultValue = "false") String expand ) {
        return employeeService.getEmployee(id,Boolean.parseBoolean(expand));
    }

    @PostMapping(value = "/employees")
    public ResponseEntity<String> addEmployees(@RequestBody Employees employees){
        employeeService.addEmployee(employees);
        return new ResponseEntity<>("Employees was added succesful",HttpStatus.OK);
    }

    @PutMapping(value = "/employees/{id}")
    public ResponseEntity<String> updateEmployee(@RequestBody Employees employees, @PathVariable Integer id){
        employeeService.updateEmployee(id,employees);
        return new ResponseEntity<>("Employee was update succesfull",HttpStatus.OK);
    }

    @DeleteMapping(value = "/employees/{id}")
    public ResponseEntity<String> deleteEmployee(@PathVariable Integer id){
        employeeService.deleteEmployee(id);
        return new ResponseEntity<>("Employee was delete succesfull",HttpStatus.OK);
    }
}
