package com.example.crudsquel.model;

import com.example.crudsquel.Interfaces.DepartmentsCommon;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DepartmentsShort implements DepartmentsCommon {
    private String departmentName;
}
