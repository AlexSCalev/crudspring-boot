package com.example.crudsquel.model;

import com.example.crudsquel.Interfaces.EmployeesCommon;
import com.example.crudsquel.validators.ContactEmailCheck;
import com.example.crudsquel.validators.ContactNumberCheck;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@DynamicUpdate
public class Employees implements EmployeesCommon {
    @Id
    @GeneratedValue
    private Integer employeeId;

    @NonNull
    @NotEmpty
    @NotBlank
    private String firstName;

    @NonNull
    @NotEmpty
    @NotBlank
    private String lastName;
    @ContactEmailCheck
    private String email;

    @ContactNumberCheck
    private String phoneNumber;
    private LocalDate hireDate;
    private String jobId;
    @Min(value = 1)
    private Double salary;
    private Double commissionPct;
    private Integer managerId;
    private Integer departmentId;

}
