package com.example.crudsquel.model;

import com.example.crudsquel.Interfaces.DepartmentsCommon;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Departments implements DepartmentsCommon {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Integer departmentId;

    @NotNull
    @NotEmpty
    @NotBlank
    private String departmentName;
    private Integer managerId;
    private Integer locationId;

}
