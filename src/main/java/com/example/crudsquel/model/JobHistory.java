package com.example.crudsquel.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "job_history")
public class JobHistory {
    @Id
    private Integer employeeId;
    private LocalDate startDate;
    private LocalDate endDate;
    private String jobId;
    private Integer departmentId;

}
