package com.example.crudsquel.service;

import com.example.crudsquel.Interfaces.DepartmentsCommon;
import com.example.crudsquel.model.Departments;
import com.example.crudsquel.model.DepartmentsShort;
import com.example.crudsquel.repository.DepartmentsRepository;
import com.example.crudsquel.repository.EmployeesRepository;
import com.example.crudsquel.repository.JobHistoryRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentService {


    private final DepartmentsRepository departmentsRepository;
    private final EmployeesRepository employeesRepository;
    private final JobHistoryRepository jobHistoryRepository;

    public DepartmentService(DepartmentsRepository departmentsRepository, EmployeesRepository employeesRepository, JobHistoryRepository jobHistoryRepository) {
        this.departmentsRepository = departmentsRepository;
        this.employeesRepository = employeesRepository;
        this.jobHistoryRepository = jobHistoryRepository;
    }

    private DepartmentsShort convertToShort(Departments departments) {
        return new DepartmentsShort(departments.getDepartmentName());
    }

    public List<DepartmentsCommon> getAllDepartments(boolean expand) {
        return (expand) ? departmentsRepository.findAll().stream()
                .map(this::convertToShort)
                .collect(Collectors.toList()) : new ArrayList<>(departmentsRepository.findAll());
    }

    public DepartmentsCommon getDepartments(Integer id, boolean expand) {
        Departments departments = departmentsRepository.findById(id).orElse(new Departments());
        return (expand) ? convertToShort(departments) : departments;
    }

    public void addDepartment(Departments departments) {
        departmentsRepository.save(departments);
    }

    public void updateDepartment(Integer id, Departments upToDate) {
        Departments oldDep = departmentsRepository.findById(id).orElse(new Departments());
        oldDep.setDepartmentName(upToDate.getDepartmentName());
        oldDep.setManagerId(upToDate.getManagerId());
        oldDep.setLocationId(upToDate.getLocationId());
        departmentsRepository.save(oldDep);
    }

    public void removeDepartment(Integer id) {
        jobHistoryRepository.removeExistingHistory(id);
        employeesRepository.setNullDepartmentIdOnDelete(id);
        jobHistoryRepository.removeAllDepartmentRelation(id);
        departmentsRepository.deleteById(id);
    }

}
