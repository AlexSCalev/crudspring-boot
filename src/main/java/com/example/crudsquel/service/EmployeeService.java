package com.example.crudsquel.service;

import com.example.crudsquel.Interfaces.EmployeesCommon;
import com.example.crudsquel.model.EmployeeShort;
import com.example.crudsquel.model.Employees;
import com.example.crudsquel.repository.DepartmentsRepository;
import com.example.crudsquel.repository.EmployeesRepository;
import com.example.crudsquel.repository.JobHistoryRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final EmployeesRepository employeesRepository;
    private final JobHistoryRepository jobHistoryRepository;
    private final DepartmentsRepository departmentsRepository;

    public EmployeeService(EmployeesRepository employeesRepository, JobHistoryRepository jobHistoryRepository, DepartmentsRepository departmentsRepository) {
        this.employeesRepository = employeesRepository;
        this.jobHistoryRepository = jobHistoryRepository;
        this.departmentsRepository = departmentsRepository;
    }

    private EmployeeShort convertToShort(Employees employees) {
        return new EmployeeShort(employees.getFirstName(), employees.getLastName(), employees.getDepartmentId());
    }

    public List<EmployeesCommon> getAllEmpoyees(boolean expand) {
        return (expand) ? employeesRepository.findAll().stream().map(this::convertToShort)
                .collect(Collectors.toList()) : new ArrayList<>(employeesRepository.findAll());
    }

    public EmployeesCommon getEmployee(Integer id, boolean expand) {
        Employees employees = employeesRepository.findById(id).orElse(new Employees());
        return (expand) ? convertToShort(employees) : employees;
    }

    public void addEmployee(Employees employees) {
        employeesRepository.save(employees);
    }

    public void updateEmployee(Integer id, Employees employees) {
        Employees empSave = employeesRepository.findById(id).orElse(new Employees());
        empSave.setFirstName(employees.getFirstName());
        empSave.setLastName(employees.getLastName());
        empSave.setEmail(employees.getEmail());
        empSave.setPhoneNumber(employees.getPhoneNumber());
        empSave.setHireDate(employees.getHireDate());
        empSave.setSalary(employees.getSalary());
        empSave.setCommissionPct(employees.getCommissionPct());
        empSave.setManagerId(employees.getManagerId());
        employeesRepository.save(empSave);
    }

    public void deleteEmployee(Integer id) {
        jobHistoryRepository.deleteAllByEmployeesId(id);
        employeesRepository.setNullManagerId(id);
        departmentsRepository.setNullableManager(id);
        employeesRepository.deleteById(id);
    }
}
