package com.example.crudsquel.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<ContactNumberCheck,String> {
    @Override
    public void initialize(ContactNumberCheck constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }
    @Override
    public boolean isValid(String number, ConstraintValidatorContext constraintValidatorContext) {
        return number != null && number.startsWith("0") && number.matches("\\d+") && number.length() == 9;
    }
}
