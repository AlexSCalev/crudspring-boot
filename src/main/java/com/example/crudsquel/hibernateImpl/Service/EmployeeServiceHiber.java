package com.example.crudsquel.hibernateImpl.Service;

import com.example.crudsquel.Interfaces.EmployeesCommon;
import com.example.crudsquel.hibernateImpl.Repository.EmployeeRepository;
import com.example.crudsquel.model.EmployeeShort;
import com.example.crudsquel.model.Employees;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceHiber {

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceHiber(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    private EmployeeShort convertToShort(Employees employees) {
        return new EmployeeShort(employees.getFirstName(), employees.getLastName(), employees.getDepartmentId());
    }

    public List<EmployeesCommon> getAllEmployees(boolean expand) {
        return (expand) ? employeeRepository.getAllEntity().stream().map(this::convertToShort)
                .collect(Collectors.toList()) : new ArrayList<>(employeeRepository.getAllEntity());
    }

    public EmployeesCommon getEmployeeById(Integer id, boolean expand) {
        Employees employees = employeeRepository.getEntityById(id);
        return (expand) ? convertToShort(employees) : employees;
    }

    public void addEmployees(Employees employees) {
        employeeRepository.addEntity(employees);
    }

    public void updateEmployee(Employees employees, Integer Id) {
        Employees employeesToUpdate = employeeRepository.getEntityById(Id);
        employeesToUpdate.setFirstName(employees.getFirstName());
        employeesToUpdate.setLastName(employees.getLastName());
        employeesToUpdate.setEmail(employees.getEmail());
        employeesToUpdate.setPhoneNumber(employees.getPhoneNumber());
        employeesToUpdate.setHireDate(employees.getHireDate());
        employeesToUpdate.setSalary(employees.getSalary());
        employeesToUpdate.setCommissionPct(employees.getCommissionPct());
        employeesToUpdate.setManagerId(employees.getManagerId());
        employeeRepository.updateEntity(employeesToUpdate);
    }

    public void deleteEmployee(Integer id) {
        employeeRepository.removeEmployee(id);
    }

}
