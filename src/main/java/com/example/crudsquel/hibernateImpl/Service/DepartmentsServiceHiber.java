package com.example.crudsquel.hibernateImpl.Service;

import com.example.crudsquel.Interfaces.DepartmentsCommon;
import com.example.crudsquel.hibernateImpl.Repository.DepartmentRepository;
import com.example.crudsquel.model.Departments;
import com.example.crudsquel.model.DepartmentsShort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentsServiceHiber {

    private final DepartmentRepository departmentsRepository;

    public DepartmentsServiceHiber(DepartmentRepository departmentsRepository) {
        this.departmentsRepository = departmentsRepository;
    }

    private DepartmentsShort convertToShort(Departments departments) {
        return new DepartmentsShort(departments.getDepartmentName());
    }

    public List<DepartmentsCommon> getAllDepartments(boolean expand) {
        return (expand) ? departmentsRepository.getAllEntity().stream().map(this::convertToShort)
                .collect(Collectors.toList()) : new ArrayList<>(departmentsRepository.getAllEntity());
    }

    public DepartmentsCommon getDepartment(Integer id, boolean expand) {
        Departments departmentToSend = departmentsRepository.getEntityById(id);
        return (expand) ? convertToShort(departmentToSend) : departmentToSend;
    }

    public void addDepartment(Departments departments) {
        departmentsRepository.addEntity(departments);
    }

    public void updateDepartments(Integer id, Departments departments) {
        departmentsRepository.updateEntity(departments, id);
    }

    public void deleteDepartments(Integer Id) {
        departmentsRepository.removeEntity(Id);
    }

}
