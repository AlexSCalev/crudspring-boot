package com.example.crudsquel.hibernateImpl.Repository;

import com.example.crudsquel.model.Employees;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManger;


    public List<Employees> getAllEntity() {
        return entityManger.createQuery("SELECT e FROM Employees e").getResultList();
    }


    public Employees getEntityById(Integer id) {
        return entityManger.find(Employees.class,id);
    }

    @Transactional
    public void addEntity(Employees entity) {
        entityManger.persist(entity);
    }

    @Transactional
    public void updateEntity(Employees entity) {
       entityManger.merge(entity);
    }

    @Transactional
    public void removeEmployee(Integer id) {
        Query queryToJobsHistory = entityManger.createQuery("DELETE FROM JobHistory WHERE employeeId = "+id);
        queryToJobsHistory.executeUpdate();
        Query queryToEmployees = entityManger.createQuery("UPDATE Employees SET managerId = null WHERE managerId ="+id);
        queryToEmployees.executeUpdate();
        Query queryToDepartment = entityManger.createQuery("UPDATE Departments SET managerId = null where managerId ="+id);
        queryToDepartment.executeUpdate();
        entityManger.remove(entityManger.find(Employees.class,id));
    }
}
