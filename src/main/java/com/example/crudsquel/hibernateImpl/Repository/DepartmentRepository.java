package com.example.crudsquel.hibernateImpl.Repository;

import com.example.crudsquel.model.Departments;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class DepartmentRepository {

    @PersistenceContext
    private EntityManager entityManger;

    @Transactional
    public List<Departments> getAllEntity() {
        return entityManger.createQuery("SELECT e FROM Departments e").getResultList();
    }

    @Transactional
    public Departments getEntityById(Integer id) {
        return entityManger.find(Departments.class,id);
    }

    @Transactional
    public void addEntity(Departments entity) {
        entityManger.persist(entity);
    }

    @Transactional
    public void updateEntity(Departments departments,Integer id) {
        Departments departmentsToUpdate = entityManger.find(Departments.class,id);
        departmentsToUpdate.setDepartmentId(departments.getDepartmentId());
        departmentsToUpdate.setDepartmentName(departments.getDepartmentName());
        departmentsToUpdate.setLocationId(departments.getLocationId());
        departmentsToUpdate.setManagerId(departments.getManagerId());
        entityManger.merge(departmentsToUpdate);
    }

    @Transactional
    public void removeEntity(Integer id) {
        Query queryToJobsHistory = entityManger.createQuery("DELETE FROM JobHistory where employeeId in (select employeeId from Employees WHERE departmentId = "+id+")");
        queryToJobsHistory.executeUpdate();
        Query queryToEmployees = entityManger.createQuery("UPDATE Employees SET departmentId = null WHERE departmentId = "+id);
        queryToEmployees.executeUpdate();
        Query queryToDepartment = entityManger.createQuery("UPDATE JobHistory SET departmentId = null where departmentId ="+id);
        queryToDepartment.executeUpdate();
        entityManger.remove(entityManger.find(Departments.class,id));
    }
}
